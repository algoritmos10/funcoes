console.log("EXERCÍCIO COM FUNÇÕES")
/* Faça uma função para determinar a média de uma turma*/

function mediaPonderada(n1, n2, n3, n4){
    let somaNotas = n1+n2*2+n3*3+n4*4
    let media = somaNotas/10
    return media
}
let aluno1, aluno2, aluno3, aluno4, aluno5
    aluno1 = mediaPonderada(70,90,75,10)
    aluno2 = mediaPonderada(10,90,75,70)
    aluno3 = mediaPonderada(10,30,45,100)
    aluno4 = mediaPonderada(95,37,63,100)
    aluno5 = mediaPonderada(83,93,70,88)
let mediaTurma = (aluno1 + aluno2 + aluno3 + aluno4 + aluno5) / 5
console.log(`À média da turma é ${mediaTurma}`)

console.log(".");

console.log("EXERCÍCIO 2")
/* Faça uma função para calcular e retornar o IMC de uma pessoa*/

function IMC(peso, altura){
    let imc = peso / altura ** 2
    return imc
}
imc = IMC(62, 1.67)
console.log(`O imc é: ${imc}`);

console.log(".")

console.log("EXERCÍCIO 3")
/* Faça uma função para determinar se um valor inteiro positivo é par*/ 

function ehPar(num){
    let result = num%2 == 0
    return result
}
console.log("53 é par? "+ehPar(53))
console.log("22 é par? "+ehPar(22))
console.log("10 é par? "+ehPar(10))
console.log("97 é par? "+ehPar(97))

console.log(".")

console.log("EXERCÍCIO 4")
/* Faça uma função para verificar se o uso de álcool compensa 
mais que o uso de gasolina. Se sim, deve-se retornar True, se não, deve-se retornar False. Sabe-se que álcool compensa se o valor do litro 
do álcool for abaixo de 70% do valor do litro de gasolina. */

function compensaAlcool(v_Alcool,v_Gasolina){
    let resultado = v_Alcool < v_Gasolina*0.7
    return resultado
}
console.log("Compensa Alcool? "+compensaAlcool(2.50 , 4.90))
console.log("Compensa Alcool? "+compensaAlcool(3.50 , 5.00))
console.log("Compensa Alcool? "+compensaAlcool(4.39 , 5.60))

console.log(".")

console.log("EXERCÍCIO 5")
/* Faça uma função para calcular a quantidade de litros de combustível gastos em viagem de carro. Sabe-se que o carro faz 12km com um litro. 
A função aceita como entrada o tempo de viagem e a valocidade média. *distância = tempo x velocidade média */

function qtsLitros(tempo, velocidade){
    let distância = tempo * velocidade
    let resultado = distância / 12
    return resultado
}
console.log(qtsLitros(1,90)+" Litros")
console.log(qtsLitros(3,105)+" Litros")
console.log(qtsLitros(0.5,70)+" Litros")
console.log(qtsLitros(8.5,105)+" Litros")

console.log(".")

console.log("EXERCÍCIO 6")
/* Faça uma função que calcula o valor de desconto para clientes. A função aceita como entrada o preço e o percentual de desconto. */

function valor_desconto(p_inteiro, desconto){
    let resultado = p_inteiro * desconto / 100
    return resultado
}
console.log(valor_desconto(180, 20)+" reais de desconto")
console.log(valor_desconto(230, 40)+" reais de desconto")
console.log(valor_desconto(129,15)+" reais de desconto")
console.log(valor_desconto(159, 25)+" reais de desconto")

console.log(".")

console.log("EXERCÍCIO 7")
/* Crie uma função para transformar medidas de:

a) km em m
b) g em kg
c) cm em m
d) em em polegadas (1 polegada = 2,54cm) */
function km_em_m(quilomêtros){
    let resultado = quilomêtros / 3.6
    return resultado
}
console.log("A) "+km_em_m(90)+" metros")

function g_em_kg(gramas){
    let resultado = gramas / 1000
    return resultado
}
console.log("B) "+g_em_kg(500)+" quilogramas")

function cm_em_m(centímetros){
    let resultado = centímetros / 100
    return resultado
}
console.log("C) "+cm_em_m(35)+" metros")

function cm_em_polegadas(cm){
    let resultado = cm * 0.39
    return resultado
}
console.log("D) "+cm_em_polegadas(78)+" polegadas")

console.log(".")

console.log("EXERCÍCIO 8")
/* Para iluminar de maneira correta os cômodos de uma casa, para cada metro quadrado deve-se usar 18W de potência. Faça uma função que receba
 as duas dimensões de um cômodo retangular (em metros), calcule a potência de iluminação que deverá ser utilizada */

function potência_d_iluminação(metros){
    let resultado = 18 * metros
    return resultado
}
console.log(potência_d_iluminação(12)+" W")
console.log(potência_d_iluminação(8)+" W")
console.log(potência_d_iluminação(22)+" W")

console.log(".")

console.log("EXERCÍCIO 9")
/* Faça uma função que receba três valores que correspondem a um horário, por exemplo, 1 hora, 40 minutos e 10 segundos, e retorne o valor 
em segundos. Neste exemplo, o retorno é 6010 segundos. */

function retorno_horário(hrs, min, seg){
    let resultado = hrs*3600 + min*60 + seg
    return resultado
}
console.log(retorno_horário(2, 13, 7)+" segundos")
console.log(retorno_horário(1, 23, 44)+" segundos")
console.log(retorno_horário(4, 5, 56)+" segundos")

console.log(".")

console.log("EXERCÍCIO 10")
/* Faça uma função que receba um valor em segundos e retorne a quantidade de horas, minutos e segundos equivalentes. 
Por exemplo, 6010 segundos são 1:40:10 */

function seg_em_hr(seg){
    let hora = (seg - seg%3600) / 3600
    let resto = seg%3600
    let min = (resto - resto%60) / 60
    let s = resto%60
    let horário = hora+":"+min+":"+s
    return horário
}
console.log(seg_em_hr(6010))

console.log(".")

console.log("EXERCÍCIO 11")
/* Faça uma função que RECEBA 2 números por parâmetro e RETORNE a soma dos mesmos */

function soma(num1, num2){
    let resultado = num1 + num2
    return resultado
}
console.log(soma(2000, 21)) //Resultado esperado -> 2021

console.log(".")

console.log("EXERCÍCIO 12")
/* Faça uma função para calcular e RETORNAR a gorjeta de um garçom. Sabe-se que um garçom recebe 10% sobre o valor consumido.*/

function gorjeta(valor){
    let resultado = 0.10*valor
    return resultado
}

console.log(gorjeta(135)) //Resultado esperado -> 13.5
console.log(gorjeta(242)) //Resultado esperado -> 24.2

console.log(".")

console.log("EXERCÍCIO 13")
/* Faça uma função e use o laço de repetição FOR para apresentar os números naturais menores que um valor inteiro positivo
 fornecido como ENTRADA. */

function mostraNumeros(){
    for(let i = 0; i < 18; i++)
    console.log(i)
}

mostraNumeros(18) //Resultado esperado -> números de 0 até 17

console.log(".")

console.log("EXERCÍCIO 14")
/* Faça uma função e use o laço de repetição WHILE para apresentar os números naturais que multiplicados por 2.5 ainda são menores 
que um valor inteiro positivo fornecido como ENTRADA.*/

function numerosMenores(n){
    let i = 0
    while (i * 2.5 < n) {
        console.log(i)
        i++
    }
}
numerosMenores(20) //Resultado esperado -> números de 0 até 7
console.log("")
numerosMenores(50) //Resultado esperado -> números de 0 até 19

console.log(".")

console.log("EXERCÍCIO 15")
/*  Faça uma função que RECEBA três números reais e RETORNE o menor. */

function menorNumero(n1, n2, n3){
    if (n1 < n2 && n1 < n3){
        return n1
    }

    if (n2 < n1 && n2 < n3){
        return n2
    }
    if (n3 < n1 && n3 < n2){
        return n3
    }
}
console.log(menorNumero(5.7,90.5,35.2)) //Resultado esperado -> 5.7
console.log(menorNumero(86.3,28.45,41.1)) //Resultado esperado -> 28.45

console.log(".")

console.log("EXERCÍCIO 16")
/* RECEBA a velocidade máxima permitida em uma avenida e a velocidade com que o motorista estava dirigindo nela, faça uma função que 
calcule a multa que uma pessoa vai receber.

Motorista deve pagar:

a) 50 reais se o motorista ultrapassar em até 10km/h a velocidade permitida;
b) 100 reais, se o motorista ultrapassar de 11 a 30 km/h a velocidade permitida.
c) 200 reais, se estiver acima de 30km/h da velocidade permitida.
*/

function calculaMulta(vel_máx, vel_motorista){
    let diferença = vel_motorista-vel_máx

    if (diferença <= 0){
        return 'Dentro do Limite'
    } else if(diferença <= 10){
        return '50 reais'
    } else if(diferença <= 30){
        return '100 reais'
    } else {
        return '200 reais'
    }
}
console.log(calculaMulta(60,67)) //Resultado esperado -> 50
console.log(calculaMulta(80,110)) //Resultado esperado -> 100
console.log(calculaMulta(110,198)) //Resultado esperado -> 200